const express = require('express');
const router = express.Router();
const key = require('../keys/keys');

var stripe = require('stripe')(key.stripeSecretKey);

router.get('/', (req, res) => {
    res.render('../views/layout/main', {
        stripePublishableKey: key.stripePublishableKey
    });
});

router.get('/success', (req, res) => {
    res.render('../views/success.ejs');
});
router.post('/charge', (req, res) => {
    const amount = 2500;
    stripe.customers.create({
            email: req.body.stripeEmail,
            source: req.body.stripeToken
        })
        .then(customer => stripe.charges.create({
            amount,
            currency: 'usd',
            description: 'Web Development Ebook',
            customer: customer.id
        }))
        .then(charge => {
            res.render('success');
        }).catch(err => {
            console.log(err);
        });
});
module.exports = router;